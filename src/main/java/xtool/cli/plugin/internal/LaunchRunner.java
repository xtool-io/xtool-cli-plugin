package xtool.cli.plugin.internal;

import java.lang.reflect.Method;

public class LaunchRunner implements Runnable {

    private final String startClassName;

    private final String[] args;

    public LaunchRunner(String startClassName, String... args) {
        this.startClassName = startClassName;
        this.args = (args != null) ? args : new String[]{};
    }

    @Override
    public void run() {
        Thread thread = Thread.currentThread();
        ClassLoader classLoader = thread.getContextClassLoader();
        try {
            Class<?> startClass = Class.forName(this.startClassName, false, classLoader);
            Method mainMethod = startClass.getMethod("main", String[].class);
            if (!mainMethod.isAccessible()) {
                mainMethod.setAccessible(true);
            }
            mainMethod.invoke(null, new Object[]{this.args});
        } catch (NoSuchMethodException ex) {
            Exception wrappedEx = new Exception(
                    "The specified mainClass doesn't contain a main method with appropriate signature.", ex);
            thread.getThreadGroup().uncaughtException(thread, wrappedEx);
        } catch (Exception ex) {
            thread.getThreadGroup().uncaughtException(thread, ex);
        }
    }


}
