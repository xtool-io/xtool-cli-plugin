package xtool.cli.plugin;

import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;
import xtool.cli.plugin.internal.IsolatedThreadGroup;
import xtool.cli.plugin.internal.LaunchRunner;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Mojo(name = "run")
public class XMojo extends AbstractMojo {

    private final String XTOOL_GROUPID = "xtool-io";

    @Component
    private PluginDescriptor pluginDescriptor;

    @Component
    private Settings settings;

    @Parameter(defaultValue = "${project}")
    private MavenProject project;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        IsolatedThreadGroup threadGroup = new IsolatedThreadGroup("org.springframework.boot.loader.JarLauncher", getLog());
        Thread launchThread = new Thread(threadGroup, new LaunchRunner("org.springframework.boot.loader.JarLauncher", createParams()), "main");
        launchThread.setContextClassLoader(new URLClassLoader(getClassPathUrls()));
        launchThread.start();
        join(threadGroup);
        threadGroup.rethrowUncaughtException();
    }

    private String[] createParams() {
        List<String> params = new ArrayList<>();
        params.add("--project.path=".concat(this.project.getBasedir().getAbsolutePath()));
        params.add("--logging.level.root=ERROR");
        params.add("--spring.config.location=file://" + project.getBasedir().getAbsolutePath().concat("/.xdef.properties"));
        return params.toArray(new String[]{});
    }

    private void join(ThreadGroup threadGroup) {
        boolean hasNonDaemonThreads;
        do {
            hasNonDaemonThreads = false;
            Thread[] threads = new Thread[threadGroup.activeCount()];
            threadGroup.enumerate(threads);
            for (Thread thread : threads) {
                if (thread != null && !thread.isDaemon()) {
                    try {
                        hasNonDaemonThreads = true;
                        thread.join();
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
        while (hasNonDaemonThreads);
    }

    protected URL[] getClassPathUrls() throws MojoExecutionException {
        try {
            List<URL> urls = new ArrayList<>();
            addXtoolDependencies(urls);
            return urls.toArray(new URL[0]);
        } catch (IOException ex) {
            throw new MojoExecutionException("Unable to build classpath", ex);
        }
    }

    private void addXtoolDependencies(List<URL> urls) throws MalformedURLException, MojoExecutionException {
//        System.out.println(pluginDescriptor.getPlugin().getDependencies());
        Dependency dep = pluginDescriptor.getPlugin().getDependencies().stream()
                .filter(cd -> cd.getGroupId().equals(XTOOL_GROUPID))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("Não foi encontrada nenhuma dependência do xtool"));
        String jarRepository = settings.getLocalRepository();
        String jarPath = dep.getGroupId().concat("/").concat(dep.getArtifactId()).concat("/").concat(dep.getVersion());
        String jarName = dep.getArtifactId().concat("-").concat(dep.getVersion()).concat(".jar");
        urls.add(new File(jarRepository.concat("/").concat(jarPath).concat("/").concat(jarName)).toURI().toURL());
    }

//    private void addXtoolCli(List<URL> urls) throws IOException {
//        Path plugins = Paths.get(settings.getLocalRepository()).resolve(XTOOL_GROUPID).resolve("plugin");
//        if (Files.exists(plugins)) {
//            urls.addAll(Files.walk(plugins)
//                    .filter(path -> path.toString().endsWith(".jar"))
//                    .map(this::convertPathToURL)
//                    .peek(url -> getLog().info("Plugin carregado: " + url))
//                    .collect(Collectors.toList()));
//        }
//    }

    private URL convertPathToURL(Path path) {
        try {
            return path.toUri().toURL();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

}
